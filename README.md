# Analyses of genomic sequences from ebola and nipah viruses

#### Marie Cariou

Analysis of illumina sequencing data for 2 ebola , 2 Nipah and 1 undetermined samples. We will first (1) map reads on a reference genome and call variations from this alignment (2) assemble *de novo* each genomes.

## 1. Data and filtration

### 1.1. Reference genomes

Reference genomes for Nipah and Ebola viruses.

```
~/2020_Reynard/data_ref/Ebola_virus.fasta:
>AF086833.2 Ebola virus - Mayinga, Zaire, 1976, complete genome
~/2020_Reynard/data_ref/Nipah_virus_isolate_UMMC1_AY029767.fasta:
>AY029767.1 Nipah virus isolate UMMC1, complete genome

```

### 1.2. Reads

There is 2 files for each sample (reads 1 and reads 2), which correspond to a paired end sequencing.
```
~/2020_Reynard$ ls data/rawFQ/
Ebola-800_S2_R1_001.fastq.gz    Ebola-800_S2_R2_001.fastq.gz                    
Ebola-911_S1_R1_001.fastq.gz    Ebola-911_S1_R2_001.fastq.gz
Nipah-CKO_S3_R1_001.fastq.gz    Nipah-CKO_S3_R2_001.fastq.gz
Nipah-WT_S4_R2_001.fastq.gz     Nipah-WT_S4_R2_001.fastq.gz
Undetermined_S0_R1_001.fastq.gz Undetermined_S0_R2_001.fastq.gz
```

**review from the sequencing plateform**
Good quality, mostly viral sequences, except for *Nipah-CKO* (bacterial contaminations)

### 1.3. Quality Check

```
~/2020_Reynard/ebola_genomics_or/src/0_quality_check.sh
```

This script runs Trimmomatic to clean raw reads.

- suppress adapters, ILLUMINACLIP:${DATA}/data/adapt.fasta:2:30:10 (files containing universal illumina adapter and monomers)
- MINLEN:100

And then rerun [Fastqc](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/).

```
ls ~/2020_Reynard/out_Trimmomatic/
```

De novo assembly and mapping and variant calling will be performed on raw or filtered reads.

## 2. Variant calling

### 2.1. Script

This script make bowtie2 indexes for both genomes and map reads for each files on these reference genome. Then, it uses different variant calling programs.

```
~/2020_Reynard/ebola_genomics_or/src/1_variant_calling_virus.sh
```

*For now, I use bcftools for variant calling. I need to try at least other classic pipeline to confirm the results, like GATK* 

### 2.2. [Bowtie mapping](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml)

Mapping outputs are in raw reads:
```
~/2020_Reynard/ebola_genomics_or/out_variant
```
results mapping des reads bruts:
```
Ebola-800_S2
26.97% overall alignment rate

Undetermined_S0
2.55% overall alignment rate

Ebola-911_S1
44.75% overall alignment rate

Nipah-CKO_S3
0.67% overall alignment rate

Undetermined_S0
3.66% overall alignment rate

Nipah-WT_S4
56.74% overall alignment rate
```

results mapping des reads filtrés par trimmomatic:
```
Ebola-800_S2
43.19% overall alignment rate

Undetermined_S0
3.22% overall alignment rate

Ebola-911_S1
70.31% overall alignment rate

Nipah-CKO_S3
1.05% overall alignment rate

Undetermined_S0
4.47% overall alignment rate

Nipah-WT_S4
76.76% overall alignment rate
```

With raw reads:
Mapping varies form 26 et 57% for ebola and Nipah, except Nipah-CKO as expected. This is coherent with the plateform figures.
2.5% and 3.7% mapping of the undetermied sample...

With reads filtered by trimmomatic, a little higher proportion of reads mapped. So we will use that.

```
~/2020_Reynard/out_variant_trimmo/
```

### 2.3. SNP calling

[bcftools](http://samtools.github.io/bcftools/bcftools.html)

Number of variants:

```
mcariou@e5-2670comp1:~/2020_Reynard/ebola_genomics_or$ grep -c -v "#" ~/2020_Reynard/out_variant_trimmo/*_dedupbcftools.vcf
/home/mcariou/2020_Reynard/out_variant_trimmo/Ebola-800_S2_ebola_dedupbcftools.vcf:1
/home/mcariou/2020_Reynard/out_variant_trimmo/Ebola-911_S1_ebola_dedupbcftools.vcf:4
/home/mcariou/2020_Reynard/out_variant_trimmo/Nipah-CKO_S3_nipah_dedupbcftools.vcf:19
/home/mcariou/2020_Reynard/out_variant_trimmo/Nipah-WT_S4_nipah_dedupbcftools.vcf:12
/home/mcariou/2020_Reynard/out_variant_trimmo/Undetermined_S0_ebola_dedupbcftools.vcf:1
/home/mcariou/2020_Reynard/out_variant_trimmo/Undetermined_S0_nipah_dedupbcftools.vcf:9
```
(*see out_variant_trimmo/ for variant calling post mapping of raw reads*)

[freebayes](https://github.com/freebayes/freebayes)

```
mcariou@e5-2670comp1:~/2020_Reynard$ grep -c -v "#" ~/2020_Reynard/out_variant_trimmo/*_dedup_freebayes.vcf
/home/mcariou/2020_Reynard/out_variant_trimmo/Ebola-800_S2_ebola_dedup_freebayes.vcf:61
/home/mcariou/2020_Reynard/out_variant_trimmo/Ebola-911_S1_ebola_dedup_freebayes.vcf:42
/home/mcariou/2020_Reynard/out_variant_trimmo/Nipah-CKO_S3_nipah_dedup_freebayes.vcf:52
/home/mcariou/2020_Reynard/out_variant_trimmo/Nipah-WT_S4_nipah_dedup_freebayes.vcf:18
/home/mcariou/2020_Reynard/out_variant_trimmo/Undetermined_S0_ebola_dedup_freebayes.vcf:115
/home/mcariou/2020_Reynard/out_variant_trimmo/Undetermined_S0_nipah_dedup_freebayes.vcf:180
```

## 3. De novo Assembly

### 3.1. Assembly Spades from raw reads

[Spades](https://cab.spbu.ru/software/spades/)

```
~/2020_Reynard/ebola_genomics_or/src/2_virus_assembly.sh
~/2020_Reynard/ebola_genomics_or/src/3_virus_assembly_eval.sh
```

Assembly outputs are in:
```
~/2020_Reynard/ebola_genomics_or/out_assembly
```

### 3.2. Results have been analysed with quast:

[QUAST](https://github.com/ablab/quast) genome assembly evaluation tool.

| Stat | Ebola 800 | Ebola 911 | Nipah CKO | Nipah WT | Undetermined |
| -------- | -------- | -------- | -------- | -------- | -------- |
| contigs (>= 0 bp)        | 244 | 1254  | 1651  | 18574 | 250 |
| contigs (>= 1000 bp)     | 8   | 17    | 4     | 124   | 26  |
| contigs (>= 5000 bp)     | 2   | 3     | 1     | 12    | 3   |
| contigs (>= 10000 bp)    | 1   | 0     | 0     | 0     | 2   |
| contigs (>= 25000 bp)    | 0   | 0     | 0     | 0     | 0   |
| contigs (>= 50000 bp)    | 0   | 0     | 0     | 0     | 0   |
| Total length (>= 0 bp)    |  97503  | 388836 | 228502 | 2644421 | 155053 |
| Total length (>= 1000 bp) |  31818  | 42026  | 8853   | 255464  | 76564  |
| Total length (>= 5000 bp) |  24395  | 21952  | 5463   | 63399   | 44369  |
| Total length (>= 10000 bp)|  18932  | 0      | 0      | 0       | 36708  |
| Total length (>= 25000 bp)|  0      | 0      | 0      | 0       | 0      |
| Total length (>= 50000 bp)|  0      | 0      | 0      | 0       | 0      |
| contigs                  | 23      | 49      | 28     | 347     | 64     |
| Largest contig           |  18932  | 8347    | 5463   | 5463    | 19044  |
| Total length             |  42561  | 62845   | 25990  | 408723  | 102456 |
| GC (%)                   |  43.63  | 42.59   | 42.65  | 39.29   | 49.88  |
| N50                      |  5463   | 1526    | 881    | 1454    | 2074   |
| N90                      |   688   | 596     | 551    | 603     | 672    |
| L50                      |   2     | 8       | 9      | 82      | 6      |
| L90                      |   16    | 38      | 24     | 274     | 47     |
| -------- | -------- | -------- | -------- | -------- | -------- |

The assembly produces several sequences called "contigs". In an ideal situation, we would like a single contig being the length of the virus genome (about 18500 bp). In reality, the virus genome might not be assembled in a single contig, and the data contains reads from other organisms which will form other, usually smaller contigs.

The stats on the above table describe to what extend the assembly is fragmented which gives an idea of its quality.

First, if we compare the 2 Ebola. The 6 first lines report the number of contigs longer than 0, 1000, 5000... base pair. Ebola 800 gives a single contig longer than 10000bp (but smaller than 25000), and only one between (5000 and 10000 bp), for a total of 244 contigs. On the other hand, Ebola 911 contains many more contigs (1254), but all lower than 5000bp. This assembly is much more fragmented.

We also can notics that the largest contigfor Ebola 8000 is 18932 bp, needs to be checked but I would bet that it is The Ebola genome. 

Nipah assemblies are very fragmented (Nipah WT is verrrryyyy large).

Undertermined assembly is quite good: a few contigs, 2 being around 18000 19000bp... Could there be 2 viruses in it? 

[cumulative plot of Ebola 800](figures/Ebola-800_S2_basic_stats/cumulative_plot.pdf)
[cumulative plot of Ebola 911](figures/Ebola-911_S1_basic_stats/cumulative_plot.pdf)
[cumulative plot of Nipah CKO](figures/Nipah-CKO_S3_basic_stats/cumulative_plot.pdf)
[cumulative plot of Nipah WT](figures/Nipah-WT_S4_basic_stats/cumulative_plot.pdf)
[cumulative plot of Undetermined](figures/Undetermined_S0_basic_stats/cumulative_plot.pdf)

### 3.3. Assembly of aligned reads

Extraction of reads mapped on reference genomes (post-trimmomatic). Assembly of filtered and mapped reads using Spades.

```
~/2020_Reynard/ebola_genomics_or/src/4_virus_assembly_postmap.sh
```
(same script to run QUAST)

**Quast output**

| Stat | Ebola 800 | Ebola 911 | Nipah CKO | Nipah WT | Undetermined-Ebola | Undetermined-Nipah
| -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| contigs (>= 0 bp)         |  1  |  2    | NA    | 3    | 2   | 1   |
| contigs (>= 1000 bp)      |  1  |  1    | NA    | 1    | 2   | 1   |
| contigs (>= 5000 bp)      |  1  |  1    | NA    | 1    | 2   | 1   |
| contigs (>= 10000 bp)     |  1  |  1    | NA    | 1    | 1   | 1   |
| contigs (>= 25000 bp)     |  0  |  0    | NA    | 0    | 0   | 0   |
| contigs (>= 50000 bp)     |  0  |  0    | NA    | 0    | 0   | 0   |
| Total length (>= 0 bp)    | 18872  | 18967 | NA | 18125 | 18567 | 17990 |
| Total length (>= 1000 bp) | 18872  | 18919 | NA | 17975 | 18567 | 17990 | 
| Total length (>= 5000 bp) | 18872  | 18919 | NA | 17975 | 18567 | 17990 |
| Total length (>= 10000 bp)| 18872  | 18919 | NA | 17975 | 11248 | 17990 |
| Total length (>= 25000 bp)|  0  |  0    | NA    | 0    | 0   | 0 |
| Total length (>= 50000 bp)|  0  |  0    | NA    | 0    | 0   | 0 |
| contigs                   |  1  |  2    | NA    | 1    | 2   | 1 |
| Largest contig            | 18872  | 18919 | NA | 17975 | 11248 | 17990 |
| Total length              | 18872  | 18919 | NA | 17975 | 18567 | 17990 |
| GC (%)                    | 41.08  | 41.07 | NA | 38.25 | 40.97 | 38.26 |
| N50                       | 18872  | 18919 | NA | 17975 | 11248 | 17990 |
| N90                       | 18872  | 18919 | NA | 17975 | 7319  | 17990 |
| L50                       |  1  |  1    | NA    | 1    |  1  | 1 |
| L90                       |  1  |  1    | NA    | 1    |  2  | 1 |
| -------- | -------- | -------- | -------- | -------- | -------- |

## 4. BLAST nt of all scaffolds for both assemblies.

```
~/2020_Reynard/ebola_genomics_or/src/5_run_blasts.sh
```
This script write and submit 5 script that blast each contigs of each assembly and fetch 1st output for each query.

**output**

```
ls ~/2020_Reynard/out_blasts/

```




