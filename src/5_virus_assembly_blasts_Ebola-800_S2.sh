#!/bin/bash
#$ -S /bin/bash
## name of the job to follow them
#$ -N blast
## name of the queue to be used
#$ -q E5-2670deb*,E5-2667v2*,E5-2667v4*
#$ -cwd
#$ -V
## where to put the log files (output and error) automatically generated by the cluster (different from the .log generated by DGINN)
## the dirs must exist before job is launched
#$ -o /home/mcariou/2020_Reynard/stdout/
#$ -e /home/mcariou/2020_Reynard/stderr/
 
### configurer l'environnement
module purge

DATA="/home/mcariou/2020_Reynard/"
ASBLY1=$DATA"/out_assembly/"
ASBLY2=$DATA"/out_assembly_postmap/"
OUT=$DATA"/out_blasts/"
DB="/Xnfs/ciridb/Cimarelli_group/blastdb/nt"

mkdir -p $OUT



query=$ASBLY1/Ebola-800_S2/contigs.fasta
echo $query
blastn -db $DB -query $query -evalue 1e-4 -outfmt 6 -out $OUT/Ebola-800_S2_asly1.blastn -max_target_seqs 1

query=$ASBLY2/Ebola-800_S2/contigs.fasta
blastn -db $DB -query $query -evalue 1e-4 -outfmt 6 -out $OUT/Ebola-800_S2_asly2.blastn -max_target_seqs 1

cat $OUT/Ebola-800_S2_asly1.blastn | awk '{print $2}' > $OUT/Ebola-800_S2_asly1.txt

    for hit in `cat $OUT/Ebola-800_S2_asly1.txt`
    do 
    echo $hit > $OUT/tmp.txt
    echo $hit >> $OUT/Ebola-800_S2_asly1_fetched2.txt

    idfetch -G $OUT/tmp.txt -t 3 | grep DEFINITION | head -1 >> $OUT/Ebola-800_S2_asly1_fetchedi2.txt
    done

cat $OUT/Ebola-800_S2_asly2.blastn | awk '{print $2}' > $OUT/Ebola-800_S2_asly2.txt

    for hit in `cat $OUT/Ebola-800_S2_asly2.txt`
    do 
    echo $hit > $OUT/tmp.txt
    echo $hit >> $OUT/Ebola-800_S2_asly2_fetched2.txt

    idfetch -G $OUT/tmp.txt -t 3 | grep DEFINITION | head -1 >> $OUT/Ebola-800_S2_asly2_fetched2.txt
    done

